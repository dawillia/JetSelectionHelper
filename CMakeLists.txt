cmake_minimum_required(VERSION 3.11)

project(JetSelectionHelper)

find_package(ROOT)
find_package(AnalysisBase)

target_compile_definitions(ROOT::Core
  INTERFACE ${ROOT_DEFINITIONS}
)

add_library(JetSelectionHelper src/JetSelectionHelper.cxx)
target_include_directories(JetSelectionHelper
  PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(JetSelectionHelper
  PRIVATE
    ROOT::Core ${ROOT_LIBRARIES}
    AnalysisBase::xAODJet
)
